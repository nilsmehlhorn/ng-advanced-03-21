import { DebugElement } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatCardModule } from '@angular/material/card';
import { By } from '@angular/platform-browser';
import { RouterTestingModule } from '@angular/router/testing';
import { BookNa } from '../models';
import { BookCardComponent } from './book-card.component';

describe(BookCardComponent.name, () => {
  describe('unit', () => {
    it('should use default content', () => {
      const component = new BookCardComponent();
      const { title, author, numPages, publisher } = component.content;
      expect(title).toBe('n/a');
      expect(author).toBe('n/a');
      expect(numPages).toBe(0);
      expect(publisher).toEqual({
        name: 'n/a',
        url: 'n/a'
      });
    });
  });

  describe('template', () => {
    let fixture: ComponentFixture<BookCardComponent>;
    let el: DebugElement;

    beforeEach(() => {
      TestBed.configureTestingModule({
        declarations: [BookCardComponent],
        imports: [MatCardModule, RouterTestingModule]
      });
      fixture = TestBed.createComponent(BookCardComponent);
      el = fixture.debugElement;
      fixture.detectChanges();
    });

    it('should create', () => {
      expect(fixture.componentInstance).toBeDefined();
    });

    it('should render title', () => {
      fixture.componentInstance.content = {
        ...new BookNa(),
        title: 'My Great Book',
        isbn: '123'
      };
      fixture.detectChanges();
      const title = el.query(By.css('[data-test="book-card-title"]'));
      const titleElement = title.nativeElement as HTMLElement;
      expect(titleElement.textContent).toBe('My Great Book');
      const detailsLinkElement = el.query(By.css('[data-test="book-card-details-link"]'))
        .nativeElement as HTMLAnchorElement;
      expect(detailsLinkElement.href).toEqual(`${location.origin}/123`);
    });
  });
});
