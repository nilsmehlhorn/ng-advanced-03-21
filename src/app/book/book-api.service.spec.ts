import { TestBed } from '@angular/core/testing';
import { BookApiService } from './book-api.service';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { BookNa } from './models';

describe(BookApiService.name, () => {
  let http: HttpTestingController;
  let service: BookApiService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [BookApiService]
    });
    http = TestBed.inject(HttpTestingController);
    service = TestBed.inject(BookApiService);
  });

  afterEach(() => {
    http.verify();
  });

  it('should return books from API', done => {
    const books = [new BookNa(), new BookNa()];
    service.getAll().subscribe(bookFromApi => {
      expect(bookFromApi).toEqual(books);
      done();
    });
    const req = http.expectOne('http://localhost:4730/books');
    req.flush(books);
  });

  it('should handle error', async () => {
    const booksPromise = service.getAll().toPromise();
    http.expectOne('http://localhost:4730/books').error(new ErrorEvent('Network down'));
    await expectAsync(booksPromise).toBeRejectedWithError('Sorry, could not load books');
  });

  it('should handle error', async () => {
    const booksPromise = service.getAll().toPromise();
    http
      .expectOne('http://localhost:4730/books')
      .flush('API Error', { status: 500, statusText: 'Internal Server Error' });
    await expectAsync(booksPromise).toBeRejectedWithError('Sorry, API error');
  });
});
