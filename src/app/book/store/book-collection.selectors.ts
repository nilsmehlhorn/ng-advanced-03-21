import { createSelector } from '@ngrx/store';
import { selectBookFeature } from './book.feature';

export const selectBookEntities = createSelector(selectBookFeature, feature => feature.bookCollection.entities);
