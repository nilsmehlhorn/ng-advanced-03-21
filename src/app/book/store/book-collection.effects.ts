import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { exhaustMap, map } from 'rxjs/operators';
import { BookApiService } from '../book-api.service';
import { loadBookComplete, loadBookStart } from './book-collection.actions';

@Injectable()
export class BookCollectionEffects {
  load$ = createEffect(() =>
    this.action$.pipe(
      ofType(loadBookStart),
      exhaustMap(() => this.bookApi.getAll()),
      map(books => loadBookComplete({ books }))
    )
  );

  constructor(private bookApi: BookApiService, private action$: Actions) {}
}
