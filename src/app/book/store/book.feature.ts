import { ActionReducerMap, createFeatureSelector } from '@ngrx/store';
import { bookCollectionReducer } from './book-collection.reducer';
import { BookCollectionSlice } from './book-collection.slice';

export interface BookState {
  bookCollection: BookCollectionSlice;
}

export const bookFeatureName = 'book';

export const selectBookFeature = createFeatureSelector<BookState>(bookFeatureName);

export const bookReducers: ActionReducerMap<BookState> = {
  bookCollection: bookCollectionReducer
};
