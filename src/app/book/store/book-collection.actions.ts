import { createAction, props } from '@ngrx/store';
import { Book } from '../models';

export const createBookStart = createAction('[Book] Create Book Start', props<{ book: Book }>());

export const loadBookStart = createAction('[Book] Load');

export const loadBookComplete = createAction('[Book] Load Complete', props<{ books: Book[] }>());
