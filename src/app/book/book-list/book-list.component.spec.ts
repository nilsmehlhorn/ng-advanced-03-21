import { Component, Input } from '@angular/core';
import { TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { of } from 'rxjs';
import { BookApiService } from '../book-api.service';
import { Book, BookNa } from '../models';
import { BookListComponent } from './book-list.component';

@Component({
  selector: 'ws-book-card',
  template: ''
})
class BookCardMockComponent {
  @Input() content: Book | undefined;
}

describe(BookListComponent.name, () => {
  let mockApi: jasmine.SpyObj<BookApiService>;

  beforeEach(() => {
    mockApi = jasmine.createSpyObj<BookApiService>(['getAll']);
    TestBed.configureTestingModule({
      declarations: [BookListComponent, BookCardMockComponent],
      providers: [
        {
          provide: BookApiService,
          useValue: mockApi
        }
      ]
    });
  });

  it('should create', () => {
    mockApi.getAll.and.returnValue(of([]));
    const fixture = TestBed.createComponent(BookListComponent);
    fixture.detectChanges();
    expect(fixture.componentInstance).toBeDefined();
  });

  it('should render books', () => {
    mockApi.getAll.and.returnValue(of([new BookNa(), new BookNa()]));
    const fixture = TestBed.createComponent(BookListComponent);
    fixture.detectChanges();
    const cards = fixture.debugElement.queryAll(By.css('[data-test="book-list-book-card"]'));
    expect(cards.length).toBe(2);
    cards.forEach(card => {
      const cardComponent = card.componentInstance as BookCardMockComponent;
      expect(cardComponent.content).toEqual(new BookNa());
    });
  });
});
