it('visit app', () => {
  cy.visit('/');
  cy.location('pathname').should('eq', '/books');
  cy.get('[data-test=nav-title]').contains('BOOK MONKEY');
});

it('should increase the number of books by 1', () => {
  const randomISBN = Math.floor(1000000000000 + Math.random() * 900000);
  cy.visit('/books');
  cy.get('[data-test="book-list-book-card"]').as('books');
  cy.get('@books').then(books => {
    cy.visit('/books/new');
    cy.get('[data-test="isbn-input"]').type(randomISBN.toString());
    cy.get('[data-test="title-input"]').type('My Great Book');
    cy.get('[data-test="author-input"]').type('Max Mustermann');
    cy.contains(/create/i).click();
    cy.location('pathname').should('eq', '/books');
    cy.get('@books').then(updatedBooks => {
      expect(updatedBooks.length).equal(books.length + 1);
      cy.request('DELETE', `http://localhost:4730/books/${randomISBN}`);
    });
  });
});
