describe('Book List', () => {
  beforeEach(() => {
    cy.intercept('http://localhost:4730', { fixture: 'books' });
    cy.visit('/books');
  });

  it('should display books', () => {
    cy.get('[data-test="book-list-book-card"]').then(books => {
      expect(books.length).equal(2);
    });
  });
});
